var dependencies = {
	madjoh_modules : [
		'styling',
		'gallery'
	],
	settings : [
		'game'
	]
};
var list = MadJohRequire.getList(dependencies);
define(list, function(require, Styling, Gallery, GameSettings){
	function setImages(selector, sourcePath){
		var element = document.querySelectorAll(selector);
		for(var i=0; i<element.length; i++) element[i].src = sourcePath;
	}
	var prefix = GameSettings.urlPrefix;

	// IMAGES FROM THE MODULES GALLERY
	setImages('.madjohButton', 			prefix + 'madjoh_modules/gallery/'+Styling.deviceSize+'/btn_madjoh.png');
	setImages('.madjohAsset', 			prefix + 'madjoh_modules/gallery/'+Styling.deviceSize+'/asset_madjoh_small.png');
	setImages('.madjohCreateButton', 	prefix + 'madjoh_modules/gallery/'+Styling.deviceSize+'/asset_madjoh_create_small.png');
	setImages('.facebookButton', 		prefix + 'madjoh_modules/gallery/'+Styling.deviceSize+'/btn_facebook.png');
	setImages('.facebookAsset', 		prefix + 'madjoh_modules/gallery/'+Styling.deviceSize+'/asset_facebook_small.png');
	setImages('#playOfflineButton', 	prefix + 'madjoh_modules/gallery/'+Styling.deviceSize+'/asset_offline_small.png');
	setImages('#ajaxLoading img', 		prefix + 'madjoh_modules/gallery/'+Styling.deviceSize+'/ajax_loading.gif');
});